﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float maxSpeed = 0.1f;
    private Rigidbody2D rigidbod;

    private Vector2 clampedDirection;

	// Use this for initialization
	void Start () {
        rigidbod = GetComponent<Rigidbody2D>();
        //transform.position = Input.GetAxis
    }

    // Update is called once per frame
    void Update() {
        updateClampedDirection();
        movePlayer();
    }

    void updateClampedDirection() {
        Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        clampedDirection = Vector2.ClampMagnitude(direction, 1f);
    }

    void movePlayer() {
        //transform.position += 
        rigidbod.position += clampedDirection* maxSpeed * Time.deltaTime;
    }
}
