﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour {

    public Text soldiersRescuedText;
    public Text soldiersHospitalText;

    private Helicopter helicopter;

    void Start() {
        helicopter = GameObject.FindGameObjectWithTag("Helicopter").GetComponent<Helicopter>();
    }

    void Update() {
        UpdateUI();
    }

    public void UpdateUI() {
        soldiersRescuedText.text = "Soldiers Rescued: " + helicopter.carryLoad;
        soldiersHospitalText.text = "Soldiers In Hospital: " + Hospital.savedSoldiers;
    }
}
