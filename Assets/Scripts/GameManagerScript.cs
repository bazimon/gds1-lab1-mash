﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

    void Update() {
        if (Input.GetKey(KeyCode.R)) {
            EndGame(false);
        }
    }

    public void EndGame(bool wonGame) {
        if (wonGame) {
            SceneManager.LoadScene("EndGameScene");
        } else {
            SceneManager.LoadScene("MainScene");
        }
    }
}
