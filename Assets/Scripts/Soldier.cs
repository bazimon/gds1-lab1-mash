﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.tag == "Helicopter") {
            coll.gameObject.GetComponent<Helicopter>().AddSoldierToHelicopter(this);
        }
    }
}
