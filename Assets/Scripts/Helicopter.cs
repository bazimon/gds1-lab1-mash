﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour {

    public int maxCarryLoad = 3;

    public int carryLoad = 0;

    public GameObject soldierPickupSFX;
    public GameObject noMoreSoldiersSFX;

    public bool AddSoldierToHelicopter(Soldier soldier) {
        if(carryLoad < maxCarryLoad) {
            ++carryLoad;
            soldier.gameObject.SetActive(false);
            Instantiate(soldierPickupSFX);
            return true;
        }
        Instantiate(noMoreSoldiersSFX);
        return false;
    }

    public int RemoveSoldiersFromTheHelicopter(){
        int savedSoldiers = carryLoad;
        carryLoad = 0;
        return savedSoldiers;
    }
}
