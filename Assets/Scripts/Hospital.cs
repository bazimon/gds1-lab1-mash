﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hospital : MonoBehaviour {

    public int maxSoldiers = 5;
    public static int savedSoldiers;
    public GameObject hospitalDepositSFX;

    void Start() {
        savedSoldiers = 0;
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.tag == "Helicopter") {
            addSoldiersToHospital(coll.gameObject.GetComponent<Helicopter>().RemoveSoldiersFromTheHelicopter());
        }
    }

    void addSoldiersToHospital(int soldierAmount) {
        if (soldierAmount > 0) {
            savedSoldiers += soldierAmount;
            Instantiate(hospitalDepositSFX);
            if (savedSoldiers >= maxSoldiers) {
                GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManagerScript>().EndGame(true);
            }
        }
    }
}
