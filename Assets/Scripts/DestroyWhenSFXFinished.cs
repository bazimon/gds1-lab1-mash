﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWhenSFXFinished : MonoBehaviour {

	// Use this for initialization
	void Start () {
        AudioSource audSourc = GetComponent<AudioSource>();
        audSourc.Play();
        StartCoroutine(DestroyAfterTime(audSourc.clip.length));
        //DestroyObject(gameObject, audSourc.clip.length);
    }

    private IEnumerator DestroyAfterTime(float time) {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
