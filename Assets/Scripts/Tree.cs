﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour {

    public GameObject treeHitSFX;

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.tag == "Helicopter") {
            Instantiate(treeHitSFX);
            GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManagerScript>().EndGame(false);
        }
    }
}
